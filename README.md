PostgreSQL Docker image with liquibase
======================================


## How to use this image

This image is intended to be used creating a derivative of it that gets built as part of your build process, incorporating the liquibase changelogs necessary for the database to run.

1. Create a folder structure like this:

```

- Dockerfile
+ src
   + scripts
      - < any maintenance scripts you'd need to run from outside the container > 
   + changelogs
      + database1
        - < liquibase files for database 1, changelog, and changesets folder >  
      + database2
        - < liquibase files for database 2, changelog, and changesets folder >  

```

Example `Dockerfile`

```
FROM epiclabs/liquipostgres:latest

ENV POSTGRES_DB=postgres
ENV POSTGRES_USER=docker
ENV POSTGRES_PASSWORD=docker
ENV MONOLITHIC_APPROACH=1

ADD src/ /

# Add command scripts
RUN chmod -R +x /scripts
```


## How to build the image:
```
docker build -t epiclabs/liquipostgres  .
```    

The container will create a database per folder in *src/changelogs/*, using the folder name as the name of the database. 
Changesets found inside these folders will be applied by Liquibase to the appropriate database.

Run example:
```
docker run -d -p 5432:5432 --name postgres -v $PWD/liquibase/changelog/:/changelog epiclabs/liquipostgres
```

It is also possible to invoke liquibase once the container is running, indicating the desired database (or databases) after the command. Available commands:
- **update** <databases>
- **generate** <databases> - creates an XML with the schema from db
- **generate_data** <databases> - creates an XML with the data from db

Example:
```
docker exec postgres /scripts/liquibase_command.sh update database1
```

Environment variables used and default values:
- `POSTGRES_DB` = `postgres`
- `POSTGRES_USER` = `docker`
- `POSTGRES_PASSWORD` = `docker`
- `MONOLITHIC_APPROACH` = `1` : A value of `1` means all tables in all databases will be created in the public schema of the postgres database. Otherwise, a different database is created per folder in the `changelogs` folder.
