#!/bin/bash

if [ "$1" == "--persistent" ];then
	mkdir testdata
	VOL="-v $PWD/testdata:/var/lib/postgresql/data"
fi

cp docker/Dockerfile .
docker build -t "testpostgres" .
docker run --rm -it $VOL testpostgres
rm Dockerfile
