#!/bin/bash
echo "Creating liquibase.properties"
: ${DB_SCHEMA_NAME:=$1}

cat <<CONF > /liquibase.properties
  driver: org.postgresql.Driver
  classpath:/usr/local/bin/postgresql-42.1.1.jre7.jar
  url: jdbc:postgresql://localhost:5432/$DB_SCHEMA_NAME
  username: $POSTGRES_USER
  password: $POSTGRES_PASSWORD
CONF
