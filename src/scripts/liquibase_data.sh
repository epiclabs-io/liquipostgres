#!/bin/bash
: ${CHANGELOG_FILE:="changelog_data_diff.xml"}

TS=$(date +%s)
echo "Generating DATA changelog ...."
liquibase --diffTypes="$DIFF_TYPES" --defaultSchemaName="$DB_SCHEMA_NAME" --changeLogFile="/changelogs/$1/$TS-$CHANGELOG_FILE" --diffTypes=data generateChangeLog

echo "Data Changelog generated into: /changelogs/$1/$TS-$CHANGELOG_FILE"
