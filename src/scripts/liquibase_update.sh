#!/bin/bash
: ${CHANGELOG_FILE:="/changelogs/$1/db.changelog.xml"}

echo "Applying changes to the database. Changelog: $CHANGELOG_FILE"
liquibase --changeLogFile="$CHANGELOG_FILE" update
