#!/bin/bash -e
# Executes a liquibase command, after checking db connection, for a set of databases

# Does not uses multiple database, just default "postgres", if this approach is used
MONOLITHIC_APPROACH=$1
shift

COMMAND=$1
shift

while test ${#} -gt 0
do
    echo "Setting up liquibase ($1)  ..."
    if [ $MONOLITHIC_APPROACH != 1 ]; then
        ./scripts/liquibase_setup.sh $1
    else
        ./scripts/liquibase_setup.sh postgres
    fi

    echo "Checking connection to db ($1) ..."
    ./scripts/liquibase_status.sh $1

    echo "Processing liquibase tasks ($1) ..."
    case "$COMMAND" in
        "update" )
            echo "Applying changelogs ($1) ..."
            ./scripts/liquibase_update.sh $1
            ;;
        "generate" )
            echo "Generating changelog ($1) ..."
            ./scripts/liquibase_generate.sh $1
            ;;
        "generate_data" )
            echo "Generating data changelog ($1) ..."
            if [ $MONOLITHIC_APPROACH != 1 ]; then
                ./scripts/liquibase_data.sh $1
            else
                ./scripts/liquibase_data.sh postgres
            fi
            ;;
    esac

    shift
done