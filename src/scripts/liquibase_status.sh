#!/bin/bash
: ${CHANGELOG_FILE:="/changelogs/$1/db.changelog.xml"}

until liquibase --changeLogFile="$CHANGELOG_FILE" status
do
	sleep 1s
	echo "Liquibase: retrying ..."
done
