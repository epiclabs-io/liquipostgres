#!/bin/bash
# From the postgres:9.4 docker image
# Modified by Siro Blanco <siro@epiclabs.io>
# Modified by Javier Peletier <jm@epiclabs.io> to add liquibase

set -e


# Creates a database per set (directory) of changesets
DATABASES_PATH=/changelogs/*

function configure_dirs() {
	
	mkdir -p "$PGDATA"
	chmod 700 "$PGDATA"
	chown -R postgres "$PGDATA"

	chmod g+s /run/postgresql
	chown -R postgres /run/postgresql

}

function initialize_data_dir() {

	eval "gosu postgres initdb $POSTGRES_INITDB_ARGS"

	# check password first so we can output the warning before postgres
	# messes it up
	if [ "$POSTGRES_PASSWORD" ]; then
		pass="PASSWORD '$POSTGRES_PASSWORD'"
		authMethod=md5
	else
		# The - option suppresses leading tabs but *not* spaces. :)
		cat >&2 <<-'EOWARN'
			****************************************************
			WARNING: No password has been set for the database.
					 This will allow anyone with access to the
					 Postgres port to access your database. In
					 Docker's default configuration, this is
					 effectively any other container on the same
					 system.
					 Use "-e POSTGRES_PASSWORD=password" to set
					 it in "docker run".
			****************************************************
		EOWARN

		pass=
		authMethod=trust
	fi

	{ echo; echo "host all all 0.0.0.0/0 $authMethod"; } >> "$PGDATA/pg_hba.conf"

}

function internal_server_start() {

	# internal start of server in order to allow set-up using psql-client		
	# does not listen on external TCP/IP and waits until start finishes
	gosu postgres pg_ctl -D "$PGDATA" \
		-o "-c listen_addresses='localhost'" \
		-w start

	: ${POSTGRES_USER:=postgres}
	: ${POSTGRES_DB:=$POSTGRES_USER}
	export POSTGRES_USER POSTGRES_DB	

}

function server_start() {

	./banner.sh "Starting PostgreSQL! Bring it on!!"
	exec gosu postgres "$@"

}

function internal_server_stop() {

	echo ""
	echo "Stopping PostgreSQL localhost-only mode..."
	
	gosu postgres pg_ctl -D "$PGDATA" -m fast -w stop

}

function create_postgres_db() {

	psql=( psql -v ON_ERROR_STOP=1 )

	if [ "$POSTGRES_DB" != 'postgres' ]; then
		"${psql[@]}" --username postgres <<-EOSQL
			CREATE DATABASE "$POSTGRES_DB" ;
		EOSQL
		echo
	fi

}

function create_flex_db() {

	# Create our own databases
	if [ $MONOLITHIC_APPROACH != 1 ]; then
		for db in $DATABASES_PATH
		do
			echo "Creating database: ${db##*/}"
			
			"${psql[@]}" --username postgres <<-EOSQL
				CREATE DATABASE "${db##*/}" ;
			EOSQL
			echo
		done
	else
		./banner.sh "/!\\    MONOLITHIC APPROACH     /!\\" \
			"Only the default <postgres> database will be created, and" \
			"the liquibase changesets will be all applied for it"
	fi

}

function create_postgres_user() {
	if [ "$POSTGRES_USER" = 'postgres' ]; then
		op='ALTER'
	else
		op='CREATE'
	fi
	"${psql[@]}" --username postgres <<-EOSQL
		$op USER "$POSTGRES_USER" WITH SUPERUSER $pass ;
	EOSQL
	echo

	psql+=( --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" )


}

function run_bundled_scripts() {

	echo
	for f in /docker-entrypoint-initdb.d/*; do
		case "$f" in
			*.sh)     echo "$0: running $f"; . "$f" ;;
			*.sql)    echo "$0: running $f"; "${psql[@]}" < "$f"; echo ;;
			*.sql.gz) echo "$0: running $f"; gunzip -c "$f" | "${psql[@]}"; echo ;;
			*)        echo "$0: ignoring $f" ;;
		esac
		echo
	done

}

function run_liquibase() {

	./banner.sh "Running Liquibase ..."

	# runs liquibase update command
	DATABASES="$(ls /changelogs/)"
	echo "Launching liquibase update. Databases:"
	echo "$DATABASES"
	/scripts/liquibase_command.sh $MONOLITHIC_APPROACH update $DATABASES 

}

function main() {

	configure_dirs

	./banner.sh "Starting Postgres in localhost-only mode to initialize db"

	# look specifically for PG_VERSION, as it is expected in the DB dir
	if [ ! -s "$PGDATA/PG_VERSION" ]; then
	
		INITIALIZING=1
		initialize_data_dir

	fi
	
	internal_server_start
	
	if ((INITIALIZING==1)); then
		
		create_postgres_db
		create_flex_db
		create_postgres_user
		run_bundled_scripts $0

		echo "------- finished database basic initialization"
	
	fi
	
	run_liquibase

	internal_server_stop

	server_start "$@"

}


# rewrite the command line to insert "postgres" in case a switch is passed
if [ "${1:0:1}" = '-' ]; then
	set -- postgres "$@"
fi

if [ "$1" = 'postgres' ]; then

	main "$@"
	
else
	exec "$@"
fi

