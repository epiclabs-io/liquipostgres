#!/bin/bash
# Cheap log banner by Javier Peletier <jm@epiclabs.io>


function repeatstring(){

	local str=$1
	local num=$2
	if ((num == 0)); then
		return 0
	fi
	printf "${str}%.0s" $(eval "echo {1.."$((num))"}")

}

function banner() {

	local PARMS=("$@")
	local maxlen=0	
	while (( ${#} > 0)) ; do
		local len=`expr length "$1"`
		if ((len > maxlen)) ; then
			maxlen=$len
		fi
		shift;
	done

	set -- "${PARMS[@]}"

	printf '\n┌'
	repeatstring "─" $(($maxlen+2))
	printf '┐\n'
	while (( ${#} > 0)) ; do
		len=`expr length "$1"`
		extra=$((maxlen-len))
		pre=$((extra / 2))

		if ((extra % 2 == 0)); then
			post=$pre
		else
			post=$((pre+1))
		fi
		
		printf "│ "
		repeatstring " " pre
		printf "$1"
		repeatstring " " post
		printf " │\n"
		shift
	done
	printf '└'
	repeatstring "─" $(($maxlen+2))
	printf '┘\n\n'

}

banner "$@"
